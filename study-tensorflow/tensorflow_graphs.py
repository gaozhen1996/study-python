import tensorflow as tf

tf.compat.v1.disable_eager_execution()


def my_fun():
    x = 2
    y = 3
    op1 = tf.add(x, y)
    op2 = tf.multiply(x, y)
    op3 = tf.pow(op2, op1)  # 计算幂

    with tf.compat.v1.Session() as sess:
        writer = tf.compat.v1.summary.FileWriter('./graphs', sess.graph)
        print(sess.run(op1))
        print(sess.run(op2))
        print(sess.run(op3))
        writer.close()


if __name__ == '__main__':
    my_fun()
