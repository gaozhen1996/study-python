import tensorflow.compat.v1 as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

tf.enable_eager_execution()
tf.disable_v2_behavior()


def my_fun():
    # 参数设置
    learning_rate = 0.01
    epoch = 10000  # 训练全量数据集的轮数

    # 生成训练数据
    df = pd.read_csv('data/data2.csv', names=['x', 'y'])
    X_data = np.asarray(np.asarray(df)[:, 0])
    Y_data = np.asarray(np.asarray(df)[:, 1])

    # 构造线型回归模型
    # tf 图的输入
    X = tf.placeholder(tf.float32, X_data.shape)
    y = tf.placeholder(tf.float32, Y_data.shape)
    # 设置模型的权重与偏置
    W = tf.Variable(np.random.randn(), name="weight")
    b = tf.Variable(np.random.randn(), name="bias")

    # 构造一个线性模型
    pred = tf.add(tf.multiply(X, W), b)

    #  损失函数设置为均方误差
    loss_op = tf.reduce_sum(tf.pow(pred - y, 2)) / (2 * X_data.shape[0])

    #  定义优化方法，随机梯度下降优化器 opt
    opt = tf.train.GradientDescentOptimizer(learning_rate)

    # 单轮训练操作 train_op
    train_op = opt.minimize(loss_op)

    # 开始训练
    with tf.Session() as sess:
        # 初始化全局变量
        sess.run(tf.global_variables_initializer())
        # 开始训练模型，
        # 因为训练集较小，所以每轮都使用全量数据训练
        for e in range(1, epoch + 1):
            sess.run(train_op, feed_dict={X: X_data, y: Y_data})
            if e % 1000 == 0:
                loss, w = sess.run([loss_op, W], feed_dict={X: X_data, y: Y_data})
                log_str = "Epoch %5d  Loss=%.4g  Model: y = %.2gx + %.2g"
                print(log_str % (e, loss, w, sess.run(b)))
        # 绘图
        plt.plot(X_data, Y_data, 'ro', label='Original data')
        plt.plot(X_data, sess.run(W) * X_data + sess.run(b), label='prediction mode line')
        plt.legend()
        plt.show()


if __name__ == '__main__':
    my_fun()
