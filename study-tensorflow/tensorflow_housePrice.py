import tensorflow.compat.v1 as tf
import pandas as pd
import numpy as np

tf.enable_eager_execution()
tf.disable_v2_behavior()


def normalize_feature(df):
    return df.apply(lambda column: (column - column.mean()) / column.std())


def my_fun():
    df1 = pd.read_csv("data/data1.csv", names=["square", "bedroom", "price"])
    df = normalize_feature(df1)
    ones = pd.DataFrame({'ones': np.ones(len(df))})  # ones是n行1列的数据框，表示x0恒为1
    df = pd.concat([ones, df], axis=1)  # 根据列合并数据
    df.head()

    X_data = np.array(df[df.columns[0:3]])
    y_data = np.array(df[df.columns[-1]]).reshape(len(df), 1)

    alpha = 0.01  # 学习率 alpha
    epoch = 500  # 训练全量数据集的轮数

    # 输入 X，形状[47, 3]
    X = tf.placeholder(tf.float32, X_data.shape)
    # 输出 y，形状[47, 1]
    y = tf.placeholder(tf.float32, y_data.shape)

    # 权重变量 W，形状[3,1]
    W = tf.get_variable("weights", (X_data.shape[1], 1), initializer=tf.constant_initializer())

    # 假设函数 h(x) = w0*x0+w1*x1+w2*x2, 其中x0恒为1
    # 推理值 y_pred  形状[47,1]
    y_pred = tf.matmul(X, W)

    # 损失函数采用最小二乘法，y_pred - y 是形如[47, 1]的向量。
    # 损失函数操作 loss
    loss_op = 1 / (2 * len(X_data)) * tf.matmul((y_pred - y), (y_pred - y), transpose_a=True)
    # 随机梯度下降优化器 opt
    opt = tf.train.GradientDescentOptimizer(learning_rate=alpha)
    # 单轮训练操作 train_op
    train_op = opt.minimize(loss_op)

    with tf.Session() as sess:
        # 初始化全局变量
        sess.run(tf.global_variables_initializer())
        # 开始训练模型，
        # 因为训练集较小，所以每轮都使用全量数据训练
        for e in range(1, epoch + 1):
            sess.run(train_op, feed_dict={X: X_data, y: y_data})
            if e % 10 == 0:
                loss, w = sess.run([loss_op, W], feed_dict={X: X_data, y: y_data})
                log_str = "Epoch %d \t Loss=%.4g \t Model: y = %.4gx1 + %.4gx2 + %.4g"
                print(log_str % (e, loss, w[1], w[2], w[0]))


if __name__ == '__main__':
    my_fun()
