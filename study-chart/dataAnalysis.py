import pandas as pd
import matplotlib.pyplot as plt
import math

plt.rcParams['font.sans-serif'] = ['SimHei']  # 显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 这两行需要手动设置


##柱状图
def chart1():
    X = [0, 1, 2, 3, 4, 5]
    Y = [222, 42, 455, 664, 454, 334]
    plt.bar(X, Y, 0.4, color="green")
    plt.xlabel("X-axis")
    plt.ylabel("Y-axis")
    plt.title("bar chart")
    plt.show()


##折线图
def chart2():
    total = 8 * math.pi
    step = 0.0001
    x_list = []
    y_list = []
    i = 0
    while i < total:
        x_list.append(i + step)
        y_list.append(math.sin(i + step))
        i = i + step
    df = pd.DataFrame(y_list, index=x_list, columns=list('A'))
    df.plot()
    plt.show()


##表格
def table():
    dataset = {
        'sites': ["Google", "Runoob", "Wiki"],
        'number': [1, 2, 3]
    }
    sity = pd.DataFrame(dataset)
    print(sity)


if __name__ == '__main__':
    chart2()
