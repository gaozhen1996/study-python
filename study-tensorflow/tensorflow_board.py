import tensorflow as tf

tf.compat.v1.disable_eager_execution()


def my_fun():
    a = tf.constant(2)
    b = tf.constant(3)
    x = tf.add(a, b)

    with tf.compat.v1.Session() as sess:
        writer = tf.compat.v1.summary.FileWriter('./graphs', sess.graph)
        print(sess.run(x))

        writer.close()


if __name__ == '__main__':
    my_fun()
