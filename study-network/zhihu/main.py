#!usr/bin/env python
# coding=utf-8
import json
import os
import time

import util
import requests
from bs4 import BeautifulSoup


# 获取动态页面返回的文本
def get(page_url):
    headers = {
        'Referer': 'https://www.zhihu.com/',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (XHTML, like Gecko) '
                      'Chrome/70.0.3538.77 Safari/537.36 ',
        "Accept-Language": "zh-CN,zh;q=0.9",
        'Cookie': 'SESSIONID=50hEHEgMmWayVQMB6k4T3RKIUF4jNwIDXAGXhdL0tWL; '
                  'JOID=UVwVA09xcXE08IsZQ3NBr3wgo4xUJRQaApnObxoOGDhtgtlQA18LGFPwiB1Hfj2cxGEs0vLGG9Fc2eufSUFqFIU=; '
                  'osd=UF8RC0xwcnU884oaR3tCrn8kq49VJhASAZjNaxINGTtpitpRAFsDG1LzjBVEfz6YzGIt0fbOGNBf3eOcSEJuHIY=; '
                  'SESSIONID=XkzM3bF9i9Z773zV7zzSVjtnJG3gnvqNxf3vE7jN05S; '
                  'JOID=Wl0cCk43f49KRTDuQDdPUwqdGntVGVCsZWsewWMZUaBpah7Ab2YK7i9FMeJMxWxCvPPBfHnuVOjUq565kSR9bFk=; '
                  'osd=V1sRA0k6eYJDQj3oTT5IXgyQE3xYH12lYmYYzGoeXKZkYxnNaWsD6SJDPOtLyGpPtfTMenTnU-XSppe-nCJwZV4=; '
                  '_zap=496ac52a-4514-4ee3-b96a-3b32fd966a9a; _xsrf=9CuwVd1gaZOGKfDOvEyW9k6mYCRWy7dI; '
                  'd_c0="ACAQp9MrlRSPTpG79T2XGznfiNamv47gzGQ=|1646388093"; _9755xjdesxxd_=32; '
                  'YD00517437729195%3AWM_TID=WOnCEQ9llX5ERBQFRRcv%2FjwDosYN0TG3; '
                  'q_c1=610b5446843349dd9033fa794987415c|1646614310000|1646614310000; '
                  'YD00517437729195%3AWM_NI=hbSHVDKxPeIJzGLPaO1zDVwlUdBgaC04dt%2BBJV%2FI%2BUHmYAZlWaf5LGgyK6WuudJBKEM7HNSjAfwUT2vsVhPVhjyRpEZmHhDRBghsF8aF9grPuHoodmMg9fn9fS3DRVEQd1M%3D; YD00517437729195%3AWM_NIKE=9ca17ae2e6ffcda170e2e6ee8de154b2b0a394e94fa3928ea3c84e868b9a83c54198ac8982f866b5b9a4d5f32af0fea7c3b92aacbe9da9d55089a8a086cc42ba9984b1e445f3a68cb7f66897bc8e88e449f4ecac95aa4a8a8ae1aaaa5f8cb397a2d96895bfaad6cf4da5a785d7d67fb7bc86a3ca4f8fe988afd26283bab9b6d73ab6ad9a89d35eacb6b7b3f34f9ab38889c75a82b9a894c534a39af788f15cb798a190e7678fb9f787ed33a68dfda5d15e878facd2d037e2a3; z_c0=2|1:0|10:1684738497|4:z_c0|80:MS4xdlFWb0NBQUFBQUFtQUFBQVlBSlZUY0ZmV0dWQS13Q19VWC0yVTAwWURjQTJEeXR5X2NjbFJnPT0=|b33cf53e28f4d6ef3e2d0f4f223d26aef7e25d4a3647ded8751b6987d2cf2884; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1684458539,1684735042,1684806772,1684891605; tst=r; SESSIONID=Sak2nBrYxnMYvUNDbxHHo0Kly6wfHpONFD2ef5O2EKv; JOID=UF8dA08xHjXTRKCCOjIt4piciRQkZ3BW5Szn_2tPdXeCNvPJcediVLVHqoU28CIkdyyR8cvEm1sqykaSDiqWYZo=; osd=W1kcAk06GDTSRquEOzMv6Z6diBYvYXFX5yfh_mpNfnGDN_HCd-ZjVr5Bq4Q0-yQldi6a98rFmVAsy0eQBSyXYJg=; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1684897302; KLBRSID=d017ffedd50a8c265f0e648afe355952|1684897325|1684891627'
    }
    try:
        r = requests.get(page_url, headers=headers, verify=False)
        if r.status_code == 200:
            r.encoding = r.apparent_encoding
            return r.text
        else:
            print('请求失败')
    except Exception as e:
        print(e)


def parse_detail(html):
    bfs = BeautifulSoup(html, 'lxml')  # 将网页源码构造成BeautifulSoup对象，方便操作
    # for b in bfs:
    #     print(b)
    print("==============================全部=====================================")
    # for b in bfs:
    #     print(b)
    print("==============================问题=====================================")
    for b in bfs.find_all('h1'):
        print(b.string)
    print("==============================描述=====================================")
    for b in bfs.find_all('span'):
        if '<span itemprop="text">' in str(b):
            print(b.string)

    seq = 1
    for b in bfs.find_all('p'):
        if "data-first-child" in str(b):
            print(f"==============================回答{seq}====================================")
            seq = seq + 1
        print(b.string)

    for a in bfs.find_all('a'):
        # print(a)
        if 'data-za-detail-view-element_name="User"' in str(a) and a.string is not None:
            print(f'src=http:{a.get("href")} context={a.string}')
        href = a.get('href')  # 获取a标签对象的href属性，即这个对象指向的链接地址
        print(a)


def get_answer_by_question_id(question_id):
    cursor = None
    fix = "&limit=10&offset=6&order=default&platform=desktop"
    fix = fix + "&include=data%5B%2A%5D.is_normal%2Cadmin_closed_comment%2Creward_info%2Cis_collapsed" \
                "%2Cannotation_action%2Cannotation_detail%2Ccollapse_reason%2Cis_sticky%2Ccollapsed_by%2Csuggest_edit" \
                "%2Ccomment_count%2Ccan_comment%2Ccontent%2Ceditable_content%2Cattachment%2Cvoteup_count" \
                "%2Creshipment_settings%2Ccomment_permission%2Ccreated_time%2Cupdated_time%2Creview_info" \
                "%2Crelevant_info%2Cquestion%2Cexcerpt%2Cis_labeled%2Cpaid_info%2Cpaid_info_content" \
                "%2Creaction_instruction%2Crelationship.is_authorized%2Cis_author%2Cvoting%2Cis_thanked%2Cis_nothelp" \
                "%2Cis_recognized%3Bdata%5B%2A%5D.mark_infos%5B%2A%5D.url%3Bdata%5B%2A%5D.author.follower_count" \
                "%2Cvip_info%2Cbadge%5B%2A%5D.topics%3Bdata%5B%2A%5D.settings.table_of_content.enabled "
    session_id = "1685155886846593173"
    url_base = f"https://www.zhihu.com/api/v4/questions/{question_id}/feeds?{fix}&session_id={session_id}"
    if cursor is not None:
        url = f'{url_base}&cursor={cursor}'
    else:
        url = url_base
    flag = True
    while flag:
        resp_list = json.loads(get(url))['data']
        if len(resp_list) < 5:
            flag = False
        for a in resp_list:
            print(a)
            cursor = a["cursor"]
            print(cursor)
            # print(a)
            print(a['target']['excerpt'])
            url = f'{url_base}&cursor={cursor}'
            a['question_id'] = question_id
            a['status'] = "0"
            util.save('zhihu_answer', a)
            time.sleep(1)


def list_question():
    session_token = "065257cd35f702785c1ac3ded96fc274"
    parm = f"action=down&ad_interval=3&after_id=12&desktop=true&page_number=3&session_token={session_token}"
    url = f"https://www.zhihu.com/api/v3/feed/topstory/recommend?{parm}"
    text = get(url)
    for a in json.loads(text)['data']:
        if 'question' in a['target'].keys():
            print(a['target']['question']['title'])
            a['status'] = '0'
            util.save('zhihu_question', a)
            time.sleep(0.5)
            # 不需要解析页面
            # question_id = a['target']['question']['id']
            # answer_id = a['target']['id']
            # print(f'https://www.zhihu.com/question/{question_id}/answer/{answer_id}')
            # resp = get(f'https://www.zhihu.com/question/{question_id}/answer/{answer_id}')
            # parse_detail(resp)


# 函数入口
if __name__ == '__main__':
    # i = 0
    # while i < 30:
    #     # 爬取问题
    #     list_question()
    #     util.question_to_deal_question()
    #     i = i +1

    # 爬取答案
    need_list = util.list_need_question()
    print(need_list)
    for t1 in need_list:
        get_answer_by_question_id(t1)
    util.update_deal_question_to_success(need_list)
