import json
import time

from elasticsearch import Elasticsearch

# 创建es连接
es = Elasticsearch(['http://127.0.0.1:9200'])


def save(index, mode):
    print(es.index(index=index, body=mode))


def query(index, body):
    if body is None:
        hits = es.search(index=index)['hits']['hits']
    else:
        hits = es.search(index=index, body=body)['hits']['hits']
    res = list()
    for hit in hits:
        res.append({'_source': hit['_source'], '_id': hit['_id']})
    return res


def question_to_deal_question():
    queryParm = {"query": {
        "term": {
            "status": '0'
        }
    }}
    deal_question = {}
    for temp in query("zhihu_question", queryParm):
        t = temp['_source']
        deal_question['id'] = t["target"]['question']['id']
        deal_question['title'] = t["target"]['question']['title']
        deal_question['excerpt'] = t["target"]['excerpt']
        deal_question['finish_status'] = '0'
        queryParm = {"query": {
            "term": {
                "id": deal_question['id']
            }
        }}
        if es.indices.exists(index='deal_question'):
            list1 = query('deal_question', queryParm)
            print(len(list1))
            if len(list1) == 0:
                save('deal_question', deal_question)
        else:
            save('deal_question', deal_question)
        temp['_source']['status'] = "1"
        es.index(index='zhihu_question', id=temp['_id'], body=temp['_source'])


def update_deal_question_to_need(id):
    queryParm = {"query": {
        "term": {
            "id": id
        }
    }}
    for temp in query("deal_question", queryParm):
        if temp['_source']['id'] == id:
            temp['_source']['finish_status'] = 'need'
            es.index(index='deal_question', id=temp['_id'], body=temp['_source'])


def update_deal_question_to_success(arr):
    for id in arr:
        queryParm = {"query": {
            "term": {
                "id": id
            }
        }}
        for temp in query("deal_question", queryParm):
            temp['_source']['finish_status'] = 'success'
            es.index(index='deal_question', id=temp['_id'], body=temp['_source'])


def list_need_question():
    queryParm = {"query": {
        "term": {
            "finish_status": 'need'
        }
    }}
    res = list()
    for t in query("deal_question", queryParm):
        temp = t['_source']
        if temp['finish_status'] == 'need':
            res.append(temp['id'])
    return res


def deal_answer():
    queryParm = {"query": {
        "term": {
            "status": "0"
        }
    }}
    for t1 in query("zhihu_answer", queryParm):
        dealAnswer = {
            'question_id': t1['_source']['target']['question']['id'],
            'question_title': t1['_source']['target']['question']['title'],
            'answer_id': t1['_source']['target']['id'],
            'answer_content': t1['_source']['target']['content'],
            "author_name": t1['_source']['target']['author']['name'],
            "author_headline": t1['_source']['target']['author']['headline']
        }
        if es.indices.exists(index='deal_answer'):
            queryParm = {"query": {
                "term": {
                    "answer_id": dealAnswer['answer_id']
                }
            }}
            list1 = query('deal_answer', queryParm)
            print(len(list1))
            print(dealAnswer['answer_id'])
            if len(list1) == 0:
                save('deal_answer', dealAnswer)
        else:
            save('deal_answer', dealAnswer)
        t1['_source']['status'] = "1"
        es.index(index='zhihu_answer', id=t1['_id'], body=t1['_source'])
        time.sleep(1)


if __name__ == '__main__':
    # print("=========================")
    # for t1 in query("deal_question", None):
    #     print(t1)
    # i = 0
    # while i< 1:
    #     question_to_deal_question()
    #     i = i+1
    # update_deal_question_to_need(486353452)
    i = 0
    while i < 20:
        deal_answer()
        i = i + 1
    # queryParm = {"query": {
    #     "term": {
    #         "answer_id": "1229998200"
    #     }
    # }}
    # list1 = query('deal_answer', queryParm)
    # for t1 in list1:
    #     print(t1)