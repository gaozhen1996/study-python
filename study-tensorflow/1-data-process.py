import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def my_fun1():
    sns.set(context="notebook", style="whitegrid", palette="dark")
    df0 = pd.read_csv('data/data0.csv', names=['square', 'price'])
    sns.lmplot(x='square', y='price',data = df0, height=6, fit_reg=True)
    df0.info()
    print(df0)
    plt.show()


def my_fun2():
    df1 = pd.read_csv('data/data1.csv', names=['square', 'bedrooms', 'price'])
    df1.head()
    # 创建一个 Axes3D object
    ax = plt.axes(projection='3d')
    # 设置 3 个坐标轴的名称
    ax.set_xlabel('square')
    ax.set_ylabel('bedrooms')
    ax.set_zlabel('price')
    # 绘制 3D 散点图
    ax.scatter3D(df1['square'], df1['bedrooms'], df1['price'], c=df1['price'], cmap='Greens')
    plt.show()


if __name__ == '__main__':
    my_fun1()